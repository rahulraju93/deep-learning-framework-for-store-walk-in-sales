-------------------------------------
Deep learning framework that tests different topologies of multi-perceptron neural networks on store walk-in data to predict the number of sales in a store.
-------------------------------------

-------------------------------------
Run 

python script.py

on any dataset with numerical values for performing regression using the deep learning framework Keras and in turn Tensor Flow.

Please contact me at rahulraju93@gmail.com if you have any doubts or questions. 

-------------------------------------