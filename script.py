import numpy
import pandas

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor

#load training data
load_data = pandas.read_csv("train_data.csv", header=None)
train_data = load_data.values

#load test data
load_data_test = pandas.read_csv("test_data.csv", header=None)
test_data = load_data_test.values

# IN is the input array, GROUND_TRUTH is the ground truth, OUT is the test dataset 

IN = train_data[:,0:11]
GROUND_TRUTH = train_data[:,11]
OUT = test_data[:,0:11]

#Deeper neural networks and extremely dense neural networks do not work well 
#Multilayer Perceptron neural networks when tuned properly work well with this regression problem and also have a very low Mean Squared Error (MSE) and standard deviation

def tuning_wide():
	# creating a wide neural network model
	wide_model = Sequential()
	#adding > 50 or < 20 increases MSE, 35 is optimal and provides a very low MSE
	wide_model.add(Dense(30, input_dim=11, init='normal', activation='relu'))
        wide_model.add(Dense(20, input_dim=11, init='normal', activation='relu'))
	wide_model.add(Dense(1, init='normal'))
	# Compile the above model
        wide_model.compile(loss='mean_squared_error', optimizer='adam')
        return wide_model

#initiate random seed for reproducibility
random_seed = 7
numpy.random.seed(random_seed)

print("Standardizing Data")
#standardizing the dataset provides more accuracy in the prediction of output for the test data
predictors = []
predictors.append(('standardize', StandardScaler()))
predictors.append(('mlp', KerasRegressor(build_fn=tuning_wide, nb_epoch=100, batch_size=5, verbose=0)))

print("Pipelining")

#pipelining predictors here 
result = Pipeline(predictors)

#fitting the data to measure performance
loss = result.fit(IN,GROUND_TRUTH)

#(Remove comment if required) print("Performance of fit:" % loss)

#array of output values for the test data
test_column = result.predict(OUT)

print ("Writing result")

with open('test_data.csv') as fin, open('result_data.csv', 'w') as fout:
        index = 0
	for line in iter(fin.readline, ''):
	    while (index < len(test_column) - 1):
		fout.write(line.replace('\n', ', ' + str(test_column[index]) + '\n'))
	    	index += 1


print ("Cross Validation")
#evalutation of the model trained 

mse_and_std = KFold(n_splits=10, random_state=random_seed)
ans = cross_val_score(result, IN, GROUND_TRUTH, cv=mse_and_std)
print("Wider: %.2f (%.2f) MSE" % (ans.mean(), ans.std()))

